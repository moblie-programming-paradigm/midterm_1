import java.util.Scanner;

public class midterm1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < A.length; i++) {
            A[i] = sc.nextInt();
        }
        A = reverseArray(A);
        for(int element : A){
            System.out.print(element + " ");
        }
    }
    
    public static int[] reverseArray(int[] A) {
        int[] list = new int[A.length];
        int index = 0;
        for (int i = A.length - 1; i >= 0; i--) {
            list[index] = A[i];
            index++;
        }
        return list;
    }

}